<?php

use Faker\Generator as Faker;

$factory->define(App\Photo::class, function (Faker $faker) {
    return [
        'votes'=> 1,
        'artistName' =>$faker->text(10),
        'piece' => $faker->text(10),
        'PieceImgePath' => $faker->text(10),
        'Details' => $faker->text(100)
    ];
});
