<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Photo;
use App\Http\Resources\Photo as PhotoResource;
use App\Events\OrderStatusChanged;
use File;
use App\ip;
use Laravel\Socialite\Facades\Socialite;
class GuestController extends Controller
{

    public function index(Request $request){
        $Photos = Photo::all();
        return PhotoResource::collection($Photos);
    }

    public function show($id){
        $Photo = Photo::findOrFail($id);

        return new PhotoResource($Photo);
    }

    public function redirect($id)
    {

        session(['photoId' => $id]);
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Return a callback method from facebook api.
     *
     * @return callback URL from facebook
     */
    public function callback(){
        $Photos = Photo::all();
        $user =  Socialite::driver('facebook')->stateless()->user();
        $photoId = session('photoId');
        $photo = Photo::findOrFail($photoId);
        if(ip::where('ipAddress',(string)$user->id)->count() == 0){
            $IP = new ip();
            $IP->ipAddress = $user->id;
            $IP->save();
            $photo->votes++;
            if($photo->save()){
                event(new OrderStatusChanged);
                return redirect('/')->with('message' , "تم عمليه التصويت بنجاح");
            }
            return redirect('/')->with('err' , "لقد حدث خطا ما الرجاء اعاده المحاوله");;
        }
        return redirect('/')->with('err' , "لقد قمت بالتصويت مسبقاً");
    }

    public function voteUp(Request $request, $id){
    }

    public function photosSorted(){
        $Photos = Photo::orderBy('votes', 'DESC')->get();

        return PhotoResource::collection($Photos);
    }
    public function delete($id){
        $Photo = Photo::findOrFail($id);
        if(File::exists($Photo->PieceImgePath)) {
            File::delete($Photo->PieceImgePath);
        }
        $Photo->delete();

        return new PhotoResource($Photo);
    }
    public function store(Request $request){

        $request->validate([
            'artistName' => 'required|max:255',
            'piece'=>'required|max:255',
            'Details'=>'required',
           'path'=>'required'
        ]);
        $Photo = new Photo;
        $Photo->votes = 0;
        $Photo->artistName = $request->artistName;
        $Photo->piece = $request->piece;
        if($request->has('path')){
          $image = $request->get('path');
          $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
          \Image::make($request->get('path'))->save(public_path('images/').$name);
          $Photo->PieceImgePath =  'images/'.$name ;
        }
        $Photo->Details = $request->Details;
        if($Photo->save()){
            return new PhotoResource($Photo);
        }
    }
    public function update(Request $request){
        $request->validate([
            'artistName' => 'required|max:255',
            'piece'=>'required|max:255',
            'Details'=>'required',
           'path'=>'required'
        ]);

        $Photo =  Photo::findOrFail($request->photo_id);
        $Photo->artistName = $request->artistName;
        $Photo->piece = $request->piece;
        if(substr($request->path,0,6) !== 'images'){
            if($request->has('path')){
                if(File::exists($Photo->PieceImgePath)) {
                    File::delete($Photo->PieceImgePath);
                }
                $image = $request->get('path');
                $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                \Image::make($request->get('path'))->save(public_path('images/').$name);
                $Photo->PieceImgePath =  'images/'.$name ;
            }
        }
        $Photo->Details = $request->Details;
        if($Photo->save()){
            return new PhotoResource($Photo);
        }
    }
}
