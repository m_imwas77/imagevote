<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Photo extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {


        return [
            'id' => $this->id,
            'votes' => $this->votes,
            'artistName' => $this->artistName,
            'piece' => $this->piece,
            'PieceImgePath' => $this->PieceImgePath,
            'Details' => $this->Details,
        ];
    }
}
