<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// List Photos
Route::get('photos', 'GuestController@index');

// show Detels of Photo
// if(Auth::check()){
    Route::get('photo/{id}', 'GuestController@show');
    Route::get('photosSorted','GuestController@photosSorted');
    Route::delete('photo/{id}', 'GuestController@delete');
    Route::post('photo', 'GuestController@store');
    Route::put('photoUpDeted', 'GuestController@update');
// }
// Route::get('/redirect', 'SocialAuthFacebookController@redirect');
// Route::get('/callback', 'SocialAuthFacebookController@callback');

// vote to photo
//Route::group(['middleware' => ['web']], function () {
    // your routes here
    Route::post('photo/vote/{id}','GuestController@voteUp');
//});


