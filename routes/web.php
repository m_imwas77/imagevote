<?php
use App\Http\Resources\Photo as PhotoResource;
use App\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;
use App\Photo;

Route::get('/', function () {
    $Photos = Photo::all();
    return view('welcome',['photos' => $Photos]);
})->name('artest');

Route::get('/ScoreBord',function(){
    return view('ScoreBord');
})->name('scoreBord');
Route::get('/ControlPanel',function(){
    if(Auth::check())
    return view('ControalPanel');
    else return view('auth.login');
})->name('controlPanel');
Route::get('/CreateNewAdmin',function(){
    if(Auth::check())
    return view('ShowAdmainForm');
    else return view('auth.login');
})->name('newAdmain');
Route::post('/CreateNewAdmin', function (Request  $request) {
    // return request()->all();
    $request->validate([
        'name' => ['required', 'string', 'max:255'],
        'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        'password' => ['required', 'string', 'min:6', 'confirmed'],
    ]);
    User::create([
        'name' =>  $request['name'],
        'email' =>  $request['email'],
        'password' => Hash::make( $request['password']),
    ]);
    return redirect('/ControlPanel');
})->name('addAdmain');
// Route::get('register', [
//     'as' => 'register',
//     'uses' => 'Auth\RegisterController@showRegistrationForm'
//   ]);
//   Route::post('register', [
//     'as' => '',
//     'uses' => 'Auth\RegisterController@register'
//   ]);
Route::get('login', [
    'as' => 'login',
    'uses' => 'Auth\LoginController@showLoginForm'
]);
Route::post('login', [
    'as' => '',
    'uses' => 'Auth\LoginController@login'
]);
Route::post('logout', [
    'as' => 'logout',
    'uses' => 'Auth\LoginController@logout'
]);

// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/redirect/{id}', ['as' => 'gopp', 'uses' => 'GuestController@redirect']);
Route::get('/callback', 'GuestController@callback');
