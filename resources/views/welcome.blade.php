@extends('layouts')

@section('contant')
<div  class="gray-bg div-padding">
    <div class="container">
    <div class="div-padding text-center" style="font-size : 25px" >
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        @if(session()->has('err'))
            <div class="alert alert-danger">
                {{ session()->get('err') }}
            </div>
        @endif
    </div>
        <div class="container-fluid" >

            <div class="row">

            </div>
            <div class="row">
                @foreach ($photos as $photo)
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="card-a">
                        <div class="box-a" style="padding:0px;">
                            <div class="blog-img" style="height: 210px;">
                                <img src="{{ $photo->PieceImgePath }}">
                            </div>
                            <h4>{{ $photo->artistName }}</h4>
                            <div class="inner-box">

                                <div >
                                    <h5> {{ $photo->piece }} </h5>
                                    <span>
                                        <div class="col-md-6">
                                            <form   class="mb-2"  method="GET"  action="{{route('gopp',['id'=>$photo->id])}}">
                                                <button href="#"  type="submit" class="btn btn-primary btn-lg" >
                                                    <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                                                </button>
                                            </form>
                                        </div>
                                        <div class="col-md-6">
                                        <button type="button" class="btn btn-primary btn-lg"  data-toggle="modal" onclick="lunch_model('{{ $photo->Details }}')" data-target="#Details">
                                                <i class="fas fa-eye"></i>
                                            </button>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>
</div>
<div class="modal" tabindex="-1" role="dialog" id="Details">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">التفاصيل</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p id="model_p">Modal body text goes here.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
</div>
<script>
    function lunch_model(text){
        var urlRegex = /(https?:\/\/[^\s]+)/g;
        text = text.replace(/\n|<br\s*\/?>/gi, "\r");

        var contant = text.replace(urlRegex, function(url) {
                return '<a href="' + url + '">' + url + '</a>';
        });
        $("#model_p").html(contant);

    }
</script>
@endsection
