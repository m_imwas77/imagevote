 @if (Auth::check())
    <div id="apps">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a  class="navbar-brand" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                    {{ __('خروج') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
                <a  class="navbar-brand" href="{{ route('controlPanel') }}">
                    {{ __('لوحه التحكم') }}
                </a>
                <a  class="navbar-brand" href="{{ route('addAdmain') }}">
                    {{ __('اضافه مسؤول جديد') }}
                </a>
                <a  class="navbar-brand" href="{{ route('artest') }}">
                    {{ __('المشاركون') }}
                </a>
                <a  class="navbar-brand" href="{{ route('scoreBord') }}">
                    {{ __('لوحه النتائج') }}
                </a>

        </nav>
    </div>
@endif
